package com.oyekidhardelivery.OtherClass;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.widget.EditText;
import android.widget.Toast;

import com.oyekidhardelivery.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 * Created by appicintel on 18/1/17.
 */

public class Utils {
    public static final int CLEAR_STACK_FLAG = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;


//    public static boolean isNetworkConnected(Context mContext) {
//        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
//
//        if (cm.getActiveNetworkInfo() != null) {
//            return true;
//        } else {
////            Utils.showSingleDialog(mContext, mContext.getString(R.string.error), mContext.getString(R.string.internet_not_connected), mContext.getString(R.string.ok), null, false);
//            return false;
//        }
//
//    }


//    public static boolean isNetworkConnectedWithoutAlert(Context mContext) {
//        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
//
//        if (cm.getActiveNetworkInfo() != null) {
//            return true;
//        } else {
//            return false;
//        }
//
//    }


//    public static String get_IMeI(Context mContext) {
//        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
//        String Imei_no = telephonyManager.getDeviceId();
//        return Imei_no;
//    }


    public static boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    public static void showToast(Context mContext, String message) {
        try {

            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        } catch (Exception e) {

        }
    }


    public static AlertDialog.Builder showDialog(Context mContext, String message, String title, String positiveText, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener, String negativeText, String neutralText, DialogInterface.OnClickListener neutralListener, boolean cancellable) {
        AlertDialog.Builder alert = getSimpleDialog(mContext);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setCancelable(cancellable);
        alert.setNegativeButton(negativeText, negativeListener);
        alert.setPositiveButton(positiveText, positiveListener);
        alert.setNeutralButton(neutralText, neutralListener);
        try {
            alert.show();
        } catch (Exception e) {
            Utils.showLog("dialog exception " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return alert;
    }

    public static AlertDialog.Builder getSimpleDialog(Context mContext) {
        return new AlertDialog.Builder(mContext);
    }

    public static AlertDialog.Builder showSingleDialog(Context mContext, String title, String message, String positiveText, DialogInterface.OnClickListener positiveListener, boolean cancellable) {
        return showDialog(mContext, message, title, positiveText, positiveListener, null, null, null, null, cancellable);
    }

    public static AlertDialog.Builder showRetryDialog(Context mContext, DialogInterface.OnClickListener positiveListener, boolean cancellable) {
        return showDialog(mContext, mContext.getString(R.string.request_failed), mContext.getString(R.string.error), mContext.getString(R.string.retry), positiveListener, null, mContext.getString(R.string.cancel), null, null, cancellable);
    }

    public static void showLog(String log) {

    }


    public static boolean isNotNull(Context mContext, EditText... et) {

        boolean valid = true;
        for (EditText editText : et) {
            if (!validEdittext(editText, mContext)) {
                valid = false;
            }
        }
        if (!valid) {
            Utils.showSingleDialog(mContext, mContext.getString(R.string.error), mContext.getString(R.string.empty_fields), mContext.getString(R.string.ok), null, true);
        }
        return valid;
    }


    public static boolean isGpsEnabled(Context mContext) {
        return ((LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean validEdittext(EditText et_reg_phone, Context mContext) {
        if (et_reg_phone.getText() == null || et_reg_phone.getText().toString().equals("")) {
            et_reg_phone.setError(mContext.getString(R.string.required_field));
//            et_reg_phone.setHintTextColor(ContextCompat.getColor(mContext, R.color.red));
            return false;
        } else {
            return true;
        }
    }

    public static boolean isMyServiceRunning(Context mContext, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void sendSms(Context mContext, String number, String message) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + number));
        sendIntent.putExtra("sms_body", message);
        mContext.startActivity(sendIntent);

    }
}
