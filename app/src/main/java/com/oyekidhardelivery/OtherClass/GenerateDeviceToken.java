package com.oyekidhardelivery.OtherClass;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by appicintel on 1/12/16.
 */
public class GenerateDeviceToken extends AsyncTask<String, Void, String> {
    //Constants for success and errors
    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public static final String REGISTRATION_ERROR = "RegistrationError";
    private static final String TAG = "RegIntentService";
    public Context context;
    private String device_token;
    private AppPreferance appPreferences;

    public GenerateDeviceToken(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {

        if (registerGCM()) {
            return "true";
        } else {
            return "false";
        }
    }


    protected void onPreExecute() {

    }

    protected void onPostExecute(String result) {
        if (result.equals("true")) {
            appPreferences = new AppPreferance();
            appPreferences.setPreferences(context, Constants.deviceTokenRsponse, device_token);

        } else {
            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
        }
    }

    private Boolean registerGCM() {

        device_token = FirebaseInstanceId.getInstance().getToken();

        // [END get_token]
        Log.i(TAG, "GCM Registration Token: " + device_token);
        if (device_token == null || device_token.isEmpty()) {
            return false;
        } else {
            return true;
        }


    }
}
