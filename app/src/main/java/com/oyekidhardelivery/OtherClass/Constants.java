package com.oyekidhardelivery.OtherClass;

/**
 * Created by appicintel on 21/1/17.
 */

public class Constants {

    //        public static String Base_Url = "http://192.168.0.189:7755/oyekidharagent/mobileservice.php?service-name=";
//    public static String Base_Url = "http://oyekidhar.com/oyekidhardelivery/mobileservice.php?service-name=";
    public static String Base_Url = "http://35.163.98.246/oyekidhardelivery/mobileservice.php?service-name=";


    public static String Register = Base_Url + "agentRegister";
    public static String login = Base_Url + "agentLogin";

    public static String ParcelDetail = Base_Url + "parceldetail";
    public static String ParcelDelivery = Base_Url + "acceptdelivery";
    public static String FinalList = Base_Url + "getfinallist";
    public static String DeviceToken = Base_Url + "updatetoken";

    public static String AgentStatusService = Base_Url + "agentstatus";
    public static String UpdateLocationService = Base_Url + "updatelocation";

    public static String Address = "http://maps.googleapis.com/maps/api/geocode/json?address=";
    public static String deviceTokenRsponse = "devicetoken";

    public static String notification_key_message = "message";
    public static String notification_key_type = "type";
    public static String notification_key_name = "title";
    public static String notification_key_number = "number";
    public static String notification_key_server_id = "server_contactId";
    public static String notification_key_image_url = "image";

    public static final String isLogin = "login";

    public static final String agentId = "agentid";


    public static String agentbranchId = "agentbranchid";


}
