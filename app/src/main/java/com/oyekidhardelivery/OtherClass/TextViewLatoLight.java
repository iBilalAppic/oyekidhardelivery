package com.oyekidhardelivery.OtherClass;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by NileshM on 21/10/16.
 */

public class TextViewLatoLight extends TextView {

    public TextViewLatoLight(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode()) {
            return;
        }

        Typeface typeface = Typeface.createFromAsset(context.getAssets(),
                "Lato-Light.ttf");
        setTypeface(typeface);
    }

}
