package com.oyekidhardelivery.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.oyekidhardelivery.OtherClass.AppPreferance;
import com.oyekidhardelivery.OtherClass.Constants;
import com.oyekidhardelivery.R;
import com.oyekidhardelivery.Services.RequestGenerator;
import com.oyekidhardelivery.Services.ResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_LOCATION = 0;
    TextView register, login;

    private static final int PERMISSION_GPS_CODE = 12;
    EditText user_mobile, user_pass, countryCode;
    String is_Login;
    String contryDialCode = null;
    String country_name = null;
    String country_Code = null;
    AppPreferance appPreferance;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_CONTACTS};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        appPreferance = new AppPreferance();

        is_Login = appPreferance.getPreferences(getApplicationContext(), Constants.isLogin);


        checkGPSPermission();

        settingsrequest();

    }

    private void checkGPSPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!allPermissionsGiven()) {
                requestPermissions(permissions, PERMISSION_GPS_CODE);
            } else {
                if (is_Login.equals("")) {
                    Log.d("login", is_Login);
                    init();
                } else {
                    Log.d("login", is_Login);
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }

        } else {
            if (is_Login.equals("")) {
                Log.d("login", is_Login);
                init();
            } else {
                Log.d("login", is_Login);
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }
    }

    private void init() {

        countryCode = (EditText) findViewById(R.id.countryCode);
        user_mobile = (EditText) findViewById(R.id.login_user);
        user_pass = (EditText) findViewById(R.id.login_password);
        register = (TextView) findViewById(R.id.register);
        login = (TextView) findViewById(R.id.login);

        LocationManager lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        Geocoder geocoder = new Geocoder(getApplicationContext());

        for (String provider : lm.getAllProviders()) {
            @SuppressWarnings("ResourceType") Location location = lm.getLastKnownLocation(provider);
            if (location != null) {
                try {
                    List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    if (addresses != null && addresses.size() > 0) {
                        country_name = addresses.get(0).getCountryName();
                        country_Code = addresses.get(0).getCountryCode();
                        getCountryDialCode(country_Code);
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (contryDialCode != null) {
            countryCode.setText(contryDialCode);
        } else {
            countryCode.setText("");
        }
        register.setOnClickListener(this);
        login.setOnClickListener(this);
        user_pass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    boolean flag = true;

                    if (user_mobile.getText().equals("")) {
                        flag = false;
                        user_mobile.setError("Phone Number Required");
                    } else if (countryCode.getText().toString().equals("")) {
                        countryCode.setError("Country Code Required");
                        flag = false;
                    } else if (user_pass.getText().equals("")) {
                        flag = false;
                        user_pass.setError("Enter Password");
                    } else if (flag) {

                        login_in();

                    } else {
                        Toast.makeText(getApplication(), "Enter Valid Cridential", Toast.LENGTH_SHORT).show();
                    }
                    handled = true;
                }
                return handled;
            }

        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean allPermissionsGiven() {

        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_GPS_CODE) {
            if (grantResults.length > 0) {
                boolean permissionGranted = false;
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(LoginActivity.this, getString(R.string.permission_not_granted), Toast.LENGTH_SHORT).show();
                        break;
                    } else {
                        permissionGranted = true;
                    }
                }

                if (permissionGranted) {
                    init();

                }
            }
        }
    }

    @Override
    public void onClick(View view) {

        if (view == register) {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);

        }
        if (view == login) {
            boolean flag = true;

            if (user_mobile.getText().equals("")) {
                flag = false;
                user_mobile.setError("Phone Number Required");
            } else if (!user_mobile.getText().toString().trim().matches("\\d{10}")) {
                user_mobile.setError("Valid Number Required");
                flag = false;
            } else if (user_pass.getText().equals("")) {
                flag = false;
                user_pass.setError("Enter Password");
            } else if (flag) {

                login_in();

            } else {
                Toast.makeText(getApplication(), "Enter Valid Cridential", Toast.LENGTH_SHORT).show();
            }
//
//
        }
    }

    private void login_in() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("countrycode", countryCode.getText().toString());
            jsonObject.put("mobile", user_mobile.getText().toString());
            jsonObject.put("password", user_pass.getText().toString());

            Log.d(" Login Request", jsonObject.toString());

            RequestGenerator.makePostRequest(this, Constants.login, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(LoginActivity.this, "Some problem occered please try again.", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    Log.d("Login Rsponse", string);
                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String loginStatus = jsonObject1.getString("status");
                        String loginMsg = jsonObject1.getString("message");

                        if (loginStatus.equals("success")) {
                            Toast.makeText(LoginActivity.this, "" + loginMsg, Toast.LENGTH_SHORT).show();

                            appPreferance.setPreferences(getApplicationContext(), Constants.isLogin, "login");
                            appPreferance.setPreferences(getApplicationContext(), Constants.agentId, jsonObject1.getString("agentid"));
                            appPreferance.setPreferences(getApplicationContext(), Constants.agentbranchId, jsonObject1.getString("agentbranchid"));

                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            intent.putExtra("agentId", jsonObject1.getString("agentid"));
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, "" + loginMsg, Toast.LENGTH_SHORT).show();
                        }

                    }

                }


            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getCountryDialCode(String contryId) {


        String[] arrContryCode = this.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < arrContryCode.length; i++) {
            String[] arrDial = arrContryCode[i].split(",");
            if (arrDial[1].trim().equals(contryId.trim())) {
                contryDialCode = arrDial[0];
                Log.e("CurrentLocationCode", contryDialCode);
                break;
            }


        }
        return contryDialCode;
    }

    public void settingsrequest() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);


        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                        try {

                            status.startResolutionForResult(LoginActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

}
