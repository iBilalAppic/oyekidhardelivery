package com.oyekidhardelivery.Activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.oyekidhardelivery.Adapter.HomeAdapter;
import com.oyekidhardelivery.ModelClass.FinalListModel;
import com.oyekidhardelivery.OtherClass.AppPreferance;
import com.oyekidhardelivery.OtherClass.Constants;
import com.oyekidhardelivery.OtherClass.GenerateDeviceToken;
import com.oyekidhardelivery.OtherClass.Utils;
import com.oyekidhardelivery.R;
import com.oyekidhardelivery.Services.AgentStatusService;
import com.oyekidhardelivery.Services.RequestGenerator;
import com.oyekidhardelivery.Services.ResponseListener;
import com.oyekidhardelivery.Services.UpdateLocation;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {


    RelativeLayout mainLayoutt;
    SwipeRefreshLayout swipeRefreshLayout;
    private Context mContext;
    private static HomeActivity instance;
    RecyclerView finalList;
    LinearLayoutManager linearLayoutManager;
    HomeAdapter adapter;
    private static final int REQUEST_LOCATION = 0;
    private BroadcastReceiver mMyBroadcastReceiver;

    ImageView logout_user;
    ArrayList<FinalListModel> arrayList = new ArrayList<>();
    TextView datee, nodata;
    private AppPreferance preferance;
    private String agentId;
    private static long back_pressed;
    private String token, notification;
    public boolean isRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        mContext = HomeActivity.this;
        instance = HomeActivity.this;
        preferance = new AppPreferance();

        agentId = preferance.getPreferences(mContext, Constants.agentId);


        settingsrequest();
        init();
//        startService(new Intent(HomeActivity.this, AgentStatusService.class));
        startService(new Intent(this, AgentStatusService.class));

    }

    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMyBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMyBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                isRunning = true;
                settingsrequest();
                getFinalData();
                generateDeviceToken();
            }
        };
        try {

            LocalBroadcastManager.getInstance(this).registerReceiver(mMyBroadcastReceiver, new IntentFilter("your_action"));

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        isRunning = true;
        settingsrequest();
        getFinalData();
        generateDeviceToken();
    }


    private void init() {
        finalList = (RecyclerView) findViewById(R.id.recyFinalList);
        linearLayoutManager = new LinearLayoutManager(this);
        finalList.setLayoutManager(linearLayoutManager);
        mainLayoutt = (RelativeLayout) findViewById(R.id.mainLayout);
        logout_user = (ImageView) findViewById(R.id.logout_user);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        datee = (TextView) findViewById(R.id.datedae);
        nodata = (TextView) findViewById(R.id.noDatatFinalList);

        logout_user.setOnClickListener(this);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("EEEE, dd");
        String formattedDate = df.format(c.getTime());
        datee.setText(formattedDate);

        getFinalData();
        generateDeviceToken();
        token = preferance.getPreferences(getApplicationContext(), Constants.deviceTokenRsponse);

        sendRegistrationToServer(token);
        swipeRefreshLayout.setOnRefreshListener(this);

    }

    private void generateDeviceToken() {

        new GenerateDeviceToken(getApplicationContext()).execute();

    }

    private void sendRegistrationToServer(String refreshedToken) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("agentid", agentId);
            jsonObject.put("devicetoken", refreshedToken);
            jsonObject.put("devicetype", "android");

            Log.d("Update Token", jsonObject.toString());

            RequestGenerator.makePostRequest1(getApplicationContext(), Constants.DeviceToken, jsonObject, false, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Utils.showToast(getApplicationContext(), "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {


                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String Status = jsonObject1.getString("status");
                        String Msg = jsonObject1.getString("msg");

                        if (Status.equals("success")) {
//                            Utils.showToast(getApplicationContext(), Msg);
                        } else {
                            Utils.showToast(getApplicationContext(), Msg);
                        }

                    }

                }


            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getFinalData() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("agentid", agentId);
            Log.d("FinalData Request", jsonObject.toString());

            RequestGenerator.makePostRequest(HomeActivity.this, Constants.FinalList, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Utils.showToast(getApplicationContext(), "Some Problem Occured ");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    Log.d("FinalData Response", string);

                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);

                        String status = jsonObject1.getString("status");
                        String msg = jsonObject1.getString("msg");

                        if (status.equals("success")) {
                            arrayList.clear();
                            JSONArray jsonArray = jsonObject1.getJSONArray("finallist");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                                FinalListModel finalListModel = new FinalListModel();
                                finalListModel.setParcelid(jsonObject2.getString("parcelid"));
                                finalListModel.setReceivername(jsonObject2.getString("receivername"));
                                finalListModel.setReceiveraddress(jsonObject2.getString("receiveraddress"));
                                finalListModel.setReceivermobile(jsonObject2.getString("receivermobile"));

                                arrayList.add(finalListModel);
                            }


                            adapter = new HomeAdapter(HomeActivity.this, arrayList);
                            finalList.setAdapter(adapter);
                            mainLayoutt.setVisibility(View.VISIBLE);
                            nodata.setVisibility(View.GONE);

                        } else {
                            mainLayoutt.setVisibility(View.GONE);
                            nodata.setVisibility(View.VISIBLE);
                            nodata.setText("No Deliveries");
                        }

                    }

                }


            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void settingsrequest() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);


        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        getLocatttion();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                        try {

                            status.startResolutionForResult(HomeActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLocatttion();
                        break;
                    case Activity.RESULT_CANCELED:
                        settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }

    private void getLocatttion() {


        if (!Utils.isMyServiceRunning(mContext, UpdateLocation.class))
            startService(new Intent(mContext, UpdateLocation.class));
    }

    @Override
    public void onClick(View v) {
        if (v == logout_user) {
            Utils.showDialog(mContext, getString(R.string.logout), getString(R.string.dial_title), getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Utils.showToast(mContext, "Successfully Logout");
                    preferance.clearSharedPreference(HomeActivity.this);
                    Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                    stopService(new Intent(getApplicationContext(), AgentStatusService.class));

                }
            }, null, getString(R.string.cancel), null, null, false);


        }
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {

            finish();
            super.onBackPressed();
        } else

        {
            Snackbar snackbar = Snackbar.make(logout_user, "Press once again to close app.", Snackbar.LENGTH_SHORT);
            View snackbarView = snackbar.getView();
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbarView.setBackgroundColor(Color.BLACK);
            snackbar.show();
            back_pressed = System.currentTimeMillis();

        }
    }

    @Override
    public void onRefresh() {

        refreshItems();
    }

    void refreshItems() {
        // Load items
        // ...

        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        settingsrequest();

        init();

        swipeRefreshLayout.setRefreshing(false);
    }

    public static HomeActivity getInstance() {

        return instance;
    }


}
