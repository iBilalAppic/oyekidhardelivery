package com.oyekidhardelivery.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.oyekidhardelivery.OtherClass.AppPreferance;
import com.oyekidhardelivery.OtherClass.Constants;
import com.oyekidhardelivery.R;
import com.oyekidhardelivery.OtherClass.Utils;
import com.oyekidhardelivery.Services.RequestGenerator;
import com.oyekidhardelivery.Services.ResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DeliveryDetails extends AppCompatActivity implements View.OnClickListener {

    ImageView bacck_butn;
    RelativeLayout manLL;
    TextView done_btn;
    String parcelIdd;
    String pos;
    int poss;
    TextView userName, userAdd, receiveemobileNumber, datee;
    private View vieww;

    AppPreferance appPreferance;
    private String parcelId, shareId;
    String agentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_details);

        appPreferance = new AppPreferance();
        agentId = appPreferance.getPreferences(DeliveryDetails.this, Constants.agentId);
        initDetail();
    }

    private void initDetail() {

        bacck_butn = (ImageView) findViewById(R.id.back_button_del_det);

        done_btn = (TextView) findViewById(R.id.done_btn);

        manLL = (RelativeLayout) findViewById(R.id.manLL);
        userName = (TextView) findViewById(R.id.getUserName);
        userAdd = (TextView) findViewById(R.id.getUserAdd);
        receiveemobileNumber = (TextView) findViewById(R.id.receiveemobileNumber);
        datee = (TextView) findViewById(R.id.dated);
        vieww = (View) findViewById(R.id.vieew);

        bacck_butn.setOnClickListener(this);

        done_btn.setOnClickListener(this);


        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("EEEE, dd");
        String formattedDate = df.format(c.getTime());


        pos = getIntent().getStringExtra("Pos");
        parcelId = getIntent().getStringExtra("parcelId");
        datee.setText(formattedDate);


        poss = Integer.parseInt(pos);
        if (poss % 2 == 0) {
            vieww.setBackgroundColor(Color.parseColor("#e9de0f"));
        } else {
            vieww.setBackgroundColor(Color.parseColor("#C14A3B"));
        }

//        manLL.setVisibility(View.GONE);
        getAlldetaforparcel();

    }

    private void getAlldetaforparcel() {

        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("parcelid", parcelId);

            RequestGenerator.makePostRequest(DeliveryDetails.this, Constants.ParcelDetail, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Utils.showToast(getApplicationContext(), "Some Problem Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    Log.d("Detail parcel Response ", string);
                    if (!string.equals("")) {

                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String msg = jsonObject1.getString("msg");

                        if (status.equals("success")) {
                            JSONArray jsonArray = jsonObject1.getJSONArray("parceldetail");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                String parcelIdd = jsonObject2.getString("parcelid");
                                userName.setText(jsonObject2.getString("receivername"));
                                userAdd.setText(jsonObject2.getString("receiveraddress"));
                                shareId = jsonObject2.getString("sharedid");

                                receiveemobileNumber.setText(jsonObject2.getString("receivermobile"));

                            }
//                            manLL.setVisibility(View.VISIBLE);

                        } else {
                            Utils.showToast(getApplicationContext(), msg);
//                            manLL.setVisibility(View.GONE);
                        }
                    }

                }


            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == done_btn) {
            sendDeliveryReport();
        }
        if (v == bacck_butn) {
            onBackPressed();
        }
    }

    private void sendDeliveryReport() {
        try {


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("parcelid", parcelId);
            jsonObject.put("agentid", agentId);
            jsonObject.put("sharedid", shareId);

            Log.d("Delivery Request", jsonObject.toString());

            RequestGenerator.makePostRequest(DeliveryDetails.this, Constants.ParcelDelivery, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {

                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    Log.d("Delivery Response", string);

                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String stattus = jsonObject1.getString("status");
                        String msg = jsonObject1.getString("msg");

                        if (stattus.equals("success")) {
                            Utils.showToast(DeliveryDetails.this, msg);
                            Intent intent = new Intent(DeliveryDetails.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            Utils.showToast(DeliveryDetails.this, msg);
                        }
                    }

                }


            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
