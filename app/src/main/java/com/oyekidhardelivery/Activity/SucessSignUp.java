package com.oyekidhardelivery.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.oyekidhardelivery.R;

public class SucessSignUp extends AppCompatActivity implements View.OnClickListener  {
    ImageView cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sucess_sign_up);

        cancel = (ImageView) findViewById(R.id.cancel);

        cancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == cancel) {
            Intent intent = new Intent(SucessSignUp.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
