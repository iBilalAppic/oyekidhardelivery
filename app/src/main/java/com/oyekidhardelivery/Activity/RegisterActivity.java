package com.oyekidhardelivery.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.oyekidhardelivery.OtherClass.AppPreferance;
import com.oyekidhardelivery.OtherClass.Constants;
import com.oyekidhardelivery.R;
import com.oyekidhardelivery.Services.RequestGenerator;
import com.oyekidhardelivery.Services.ResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    CircleImageView img_ProfileImage;
    TextView signUp, fullTym, partTym;
    private Uri mImageCaptureUri;

    EditText name, pincode, address_one, address_two, cityname, state, phone, email;
    String sCity, sState, sCountry;
    ;

    Bitmap bitmap = null;
    String path = "";
    String putEditImage = "";
    Bitmap bm;
    byte[] b;
    ByteArrayOutputStream baos;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;
    AppPreferance appPreferance;

    int flag = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        signUp = (TextView) findViewById(R.id.signUp);

        name = (EditText) findViewById(R.id.regName);
        pincode = (EditText) findViewById(R.id.regPin);
        address_one = (EditText) findViewById(R.id.regAdd1);
        address_two = (EditText) findViewById(R.id.regAdd2);
        cityname = (EditText) findViewById(R.id.regCity);
        state = (EditText) findViewById(R.id.regState);
        fullTym = (TextView) findViewById(R.id.fullTym);
        partTym = (TextView) findViewById(R.id.partTym);
        phone = (EditText) findViewById(R.id.regPhone);
        email = (EditText) findViewById(R.id.regEmail);

        appPreferance = new AppPreferance();
        pincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pincode.getText().toString();
                if (pincode.getText().toString().length() == 6) {

                    getCityStat();
                }
                if (pincode.getText().toString().length() < 6) {
                    cityname.setText("");
                    state.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        img_ProfileImage = (CircleImageView) findViewById(R.id.edt_ProfileImage);
        signUp.setOnClickListener(this);
        img_ProfileImage.setOnClickListener(this);
        fullTym.setOnClickListener(this);
        partTym.setOnClickListener(this);
    }

    private void getCityStat() {
        JSONObject jsonObject = new JSONObject();
        String url = Constants.Address + pincode.getText().toString() + "&sensor=true";

        RequestGenerator.makePostRequest(this, url, jsonObject, true, new ResponseListener() {
            @Override
            public void onError(VolleyError error) {
                Toast.makeText(RegisterActivity.this, "Eror", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String string) throws JSONException {
                Log.d("City&State", string);

                JSONObject jsonObject = new JSONObject(string);

                if (jsonObject.getString("status").equals("OK")) {

                    JSONArray jsonArray = jsonObject.getJSONArray("results");
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (i == 0) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                JSONArray jsonArray1 = jsonObject1.getJSONArray("address_components");
                                if (jsonArray1.length() > 0) {
                                    for (int j = 0; j < jsonArray1.length(); j++) {
                                        JSONObject jsonObject2 = jsonArray1.getJSONObject(j);
                                        if (j == jsonArray1.length() - 3) {

                                            sCity = jsonObject2.getString("long_name");

                                            cityname.setText(sCity);

                                        }

                                        if (j == jsonArray1.length() - 2) {
                                            sState = jsonObject2.getString("long_name");

                                            state.setText(sState);
                                        }

                                        if (j == jsonArray1.length() - 1) {

                                            sCountry = jsonObject2.getString("long_name");
                                        }

                                    }
                                }

                            }
                        }
                    }
                }
            }


        });
    }

    private void selectImage() {

        final String[] items = new String[]{"From Camera", "From SD Card"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(),
                            "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                    mImageCaptureUri = Uri.fromFile(file);

                    try {
                        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                        intent.putExtra("return-data", true);

                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dialog.cancel();
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);

                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);

                }
            }
        });

        final AlertDialog dialog = builder.create();

        img_ProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;


        if (requestCode == PICK_FROM_FILE) {
            mImageCaptureUri = data.getData();
            path = getRealPathFromURI(mImageCaptureUri); //from Gallery

            if (path == null) {

                path = mImageCaptureUri.getPath(); //from File Manager
                bm = BitmapFactory.decodeFile(path);
                baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                b = baos.toByteArray();

                putEditImage = Base64.encodeToString(b, Base64.DEFAULT);


            } else if (path != null) {

                bitmap = BitmapFactory.decodeFile(path);
                bm = BitmapFactory.decodeFile(path);
                baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                b = baos.toByteArray();

                putEditImage = Base64.encodeToString(b, Base64.DEFAULT);
                Log.d("path1", putEditImage);


            }
        } else {
            path = mImageCaptureUri.getPath();
            bitmap = BitmapFactory.decodeFile(path);

            bm = BitmapFactory.decodeFile(path);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            b = baos.toByteArray();

            putEditImage = Base64.encodeToString(b, Base64.DEFAULT);
            Log.d("path2", putEditImage);

        }

        img_ProfileImage.setImageBitmap(bitmap);
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);

        if (cursor == null) return null;

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        return cursor.getString(column_index);
    }


    @Override
    public void onClick(View view) {


        if (view == img_ProfileImage) {
            selectImage();
        }
        if (view == fullTym) {
            flag = 1;
            partTym.setBackgroundColor(Color.WHITE);
            partTym.setTextColor(Color.BLACK);
            fullTym.setBackgroundColor(Color.GRAY);
            fullTym.setTextColor(Color.parseColor("#ff8008"));
        }
        if (view == partTym) {
            flag = 2;
            fullTym.setBackgroundColor(Color.WHITE);
            partTym.setBackgroundColor(Color.GRAY);
            partTym.setTextColor(Color.parseColor("#ff8008"));
            fullTym.setTextColor(Color.BLACK);
        }


        if (view == signUp) {

            if (name.getText().toString().equals("")) {

                name.setError("Name required");
                name.requestFocus();
            } else if (pincode.getText().toString().equals("")) {

                pincode.setError("Pincode required");
                pincode.requestFocus();

            } else if (!pincode.getText().toString().trim().matches("\\d{6}")) {
                pincode.setError("Enter valid 6 digit Pincode");
                pincode.requestFocus();
            } else if (address_one.getText().toString().equals("")) {
                address_one.setError("address required");
                address_one.requestFocus();
            } else if (address_two.getText().toString().equals("")) {
                address_two.setError("address required");
                address_two.requestFocus();
            } else if (cityname.getText().toString().equals("")) {
                cityname.setError("city required");
                cityname.requestFocus();
            } else if (state.getText().toString().equals("")) {
                state.setError("state required");
                state.requestFocus();
            } else if (flag == 0) {
                Toast.makeText(this, "please choose shift type", Toast.LENGTH_SHORT).show();

            } else if (phone.getText().toString().equals("")) {
                phone.setError("Phone number required");
                phone.requestFocus();
            } else if (!phone.getText().toString().trim().matches("\\d{10}")) {
                phone.setError("Valid 10 Number required");
                phone.requestFocus();

            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                email.setError("Enter valid Email");
                email.requestFocus();
            } else {
                signUp_now();

            }


        }
    }

    private void signUp_now() {

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", name.getText().toString());
            jsonObject.put("pincode", pincode.getText().toString());
            jsonObject.put("address1", address_one.getText().toString());
            jsonObject.put("address2", address_two.getText().toString());
            jsonObject.put("city", cityname.getText().toString());
            jsonObject.put("state", state.getText().toString());

            if (flag == 1) {
                jsonObject.put("shift_time", "Full Time");
            } else if (flag == 2) {
                jsonObject.put("shift_time", "Part Time");
            }
            jsonObject.put("mobile", phone.getText().toString());
            jsonObject.put("email", email.getText().toString());
            Log.d(" Signup Request", jsonObject.toString());


            RequestGenerator.makePostRequest(this, Constants.Register, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(RegisterActivity.this, "Some problem occered please try again.", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    Log.d("Signup Rsponse", string);
                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        String regisStatus = jsonObject1.getString("status");
                        String regisMsg = jsonObject1.getString("message");

                        if (regisStatus.equals("success")) {
                            appPreferance.setPreferences(getApplicationContext(), Constants.isLogin, "login");
                            Toast.makeText(RegisterActivity.this, "" + regisMsg, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RegisterActivity.this, SucessSignUp.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(RegisterActivity.this, "" + regisMsg, Toast.LENGTH_SHORT).show();
                        }


                    }

                }


            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}