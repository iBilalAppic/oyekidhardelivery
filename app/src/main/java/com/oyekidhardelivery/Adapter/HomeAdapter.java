package com.oyekidhardelivery.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oyekidhardelivery.Activity.DeliveryDetails;
import com.oyekidhardelivery.Activity.HomeActivity;
import com.oyekidhardelivery.ModelClass.FinalListModel;
import com.oyekidhardelivery.R;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 18/3/17.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.FinalListUi> {
    Context context;
    ArrayList<FinalListModel> arrayList = new ArrayList<>();

    public HomeAdapter(HomeActivity context, ArrayList<FinalListModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public FinalListUi onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        if (viewType == 1) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.final_recy_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.final_recy_list2, parent, false);
        }

        return new FinalListUi(itemView);
    }


    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {

            return 1;
        } else {
            return 2;
        }

    }

    @Override
    public void onBindViewHolder(FinalListUi holder, int position) {

        holder.userName.setText(arrayList.get(position).getReceivername());
        holder.userAdd.setText(arrayList.get(position).getReceiveraddress());
        holder.usermobile.setText(arrayList.get(position).getReceivermobile());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class FinalListUi extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout allLayout, mainLL;
        TextView userName, userAdd, usermobile;

        public FinalListUi(View itemView) {
            super(itemView);


            mainLL = (LinearLayout) itemView.findViewById(R.id.mainLayouut);
            allLayout = (LinearLayout) itemView.findViewById(R.id.allUi);
            userName = (TextView) itemView.findViewById(R.id.userName);
            userAdd = (TextView) itemView.findViewById(R.id.userAdd);
            usermobile = (TextView) itemView.findViewById(R.id.usermobileNumber);
            itemView.setOnClickListener(this);
            mainLL.setOnClickListener(this);
            userName.setOnClickListener(this);
            userAdd.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (v == mainLL || v == userName || v == userAdd) {
                int position = getPosition();

                Intent intent = new Intent(context, DeliveryDetails.class);
                intent.putExtra("parcelId", arrayList.get(position).getParcelid());
                intent.putExtra("Pos", String.valueOf(position));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        }
    }


}
