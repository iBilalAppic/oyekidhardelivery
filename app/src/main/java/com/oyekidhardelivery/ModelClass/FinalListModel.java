package com.oyekidhardelivery.ModelClass;

/**
 * Created by Bilal Khan on 18/3/17.
 */

public class FinalListModel {

    String parcelid, receivername, receiveraddress, receivermobile;

    public String getParcelid() {
        return parcelid;
    }

    public void setParcelid(String parcelid) {
        this.parcelid = parcelid;
    }

    public String getReceivername() {
        return receivername;
    }

    public void setReceivername(String receivername) {
        this.receivername = receivername;
    }

    public String getReceiveraddress() {
        return receiveraddress;
    }

    public void setReceiveraddress(String receiveraddress) {
        this.receiveraddress = receiveraddress;
    }

    public String getReceivermobile() {
        return receivermobile;
    }

    public void setReceivermobile(String receivermobile) {
        this.receivermobile = receivermobile;
    }
}
