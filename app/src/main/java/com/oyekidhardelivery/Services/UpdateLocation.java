package com.oyekidhardelivery.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.VolleyError;
import com.oyekidhardelivery.OtherClass.AppPreferance;
import com.oyekidhardelivery.OtherClass.Constants;
import com.oyekidhardelivery.OtherClass.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bilal Khan on 23/3/17.
 */

public class UpdateLocation extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Context mContext;

    private int PERMISSION_REQUEST_CODE = 23;
    private GoogleApiClient mGoogleApiClient;
    private int GPS_DISABLE_NOTICATION_ID = 0x132432;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    private AppPreferance appPreferance;


    @Override
    public void onCreate() {
        super.onCreate();

        appPreferance = new AppPreferance();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Utils.showLog("Service Started");
        mContext = this;
        if (Utils.isGpsEnabled(mContext)) {
            basicVariablesInit();
        } else {
            this.stopSelf();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.showLog("Service Stopped");

        stopLocationUpdates();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    private void basicVariablesInit() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        checkLocationSettings();
    }

    private void checkLocationSettings() {
        if (!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();


        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        final PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                        showNotification(getString(R.string.enable_gps_notification_message), GPS_DISABLE_NOTICATION_ID);
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                        showNotification(getString(R.string.enable_gps_notification_message), GPS_DISABLE_NOTICATION_ID);
                        break;
                }
            }
        });
    }


    @SuppressWarnings("MissingPermission")
    protected void startLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {

                }
            });

        }
    }

    private void stopLocationUpdates() {

        if (mGoogleApiClient.isConnected()) {

            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                }
            });
            mGoogleApiClient.disconnect();
        }
    }

    @SuppressWarnings("MissingPermission")
    private void getLocation(Location location) {
        if (location != null) {

            Double sLat = location.getLatitude();
            Double sLong = location.getLongitude();
//            String sLat = String.valueOf(location.getLatitude());
//            String sLong = String.valueOf(location.getLongitude());

            Utils.showLog(sLat + "," + sLong);
//            if (Utils.isUserLoggedIn(mContext))

            if (!appPreferance.getPreferences(getApplicationContext(), Constants.isLogin).equals("")) {
                sendLocationToServer(sLat, sLong);
            }
        }
    }

    private void sendLocationToServer(final Double lat, final Double lng) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() != null) if (cm.getActiveNetworkInfo() != null) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("agentid", appPreferance.getPreferences(mContext, Constants.agentId));
                jsonObject.put("latitude", lat);
                jsonObject.put("longitude", lng);
                Utils.showLog("Location Updated to ---> " + lat + "," + lng);
                Log.d("Location Update Request", jsonObject.toString());
                RequestGenerator.makePostRequest1(mContext, Constants.UpdateLocationService, jsonObject, true, new ResponseListener() {
                    @Override
                    public void onError(VolleyError error) {
//                        Utils.showToast(mContext, getString(R.string.internet_not_connected));
                    }

                    @Override
                    public void onSuccess(String string) throws JSONException {
                        if (!string.equals("")) {
                            JSONObject jsonObject1 = new JSONObject(string);
                            String status = jsonObject1.getString("status");
                            String msg = jsonObject1.getString("msg");
                            if (status.equals("success")) {

                                Utils.showLog("Location Updated to ---> " + lat + "," + lng);
                            } else {
                                Utils.showToast(mContext, msg);
                            }
                        } else {
//                            Utils.showToast(mContext, getString(R.string.internet_not_connected));
                        }
                    }


                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
//            Utils.showToast(mContext, getString(R.string.internet_not_connected));
        }
    }

//    private void showNotification(String message, int GPS_DISABLE_NOTICATION_ID) {
//
//        if (mGoogleApiClient != null)
//            stopLocationUpdates();
//
//        stopSelf();
//        if (Utils.isUserLoggedIn(mContext)) {
//
//            Intent intent = new Intent(this, HomeActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
//                    PendingIntent.FLAG_ONE_SHOT);
//
//            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.drawable.ic_notifcation_small)
//                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
//                    .setContentTitle(this.getString(R.string.app_name))
//                    .setContentText(message)
//                    .setStyle(new NotificationCompat.BigTextStyle()
//                            .bigText(message))
//                    .setAutoCancel(true)
//                    .setSound(defaultSoundUri)
//                    .setContentIntent(pendingIntent);
//
//            NotificationManager notificationManager =
//                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//            if (GPS_DISABLE_NOTICATION_ID == 0) {
//                int notificationID = (int) ((Calendar.getInstance().getTime().getTime() / 1000L) % Integer.MAX_VALUE);
//                notificationManager.notify(notificationID, notificationBuilder.build());
//            } else {
//                notificationManager.notify(GPS_DISABLE_NOTICATION_ID, notificationBuilder.build());
//            }
//        }
//    }

    @SuppressWarnings("MissingPermission")
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        getLocation(mLastLocation);
        Utils.showLog("GoogleApi connection connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Utils.showLog("GoogleApi connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Utils.showLog("GoogleApi connection failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        getLocation(location);
    }


}
