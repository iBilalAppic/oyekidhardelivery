package com.oyekidhardelivery.Services;

import android.util.Log;

import com.oyekidhardelivery.OtherClass.AppPreferance;
import com.oyekidhardelivery.OtherClass.Constants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Bilal Khan on 28/3/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    AppPreferance appPreferance;
    private String agentId;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        appPreferance = new AppPreferance();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("Registration Token", "Refreshed token: " + refreshedToken);
        agentId = appPreferance.getPreferences(this, Constants.agentId);
        appPreferance.setPreferences(this, Constants.deviceTokenRsponse, refreshedToken);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

    }


}
