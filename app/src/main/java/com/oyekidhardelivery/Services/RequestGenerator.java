package com.oyekidhardelivery.Services;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oyekidhardelivery.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bilal on 30/11/2016.
 */
public class RequestGenerator {

    private static ProgressDialog progressDialog;
    private static int COUNT;
    static AlertDialog customAlertDialog;

    public static void showProgressDialog(final Activity context) {
        try {
            cancelProgressDialog();
            context.runOnUiThread(new Runnable() {
                public void run() {
//                    progressDialog = new ProgressDialog(context);
//                    progressDialog.setMessage("Please wait...");
//                    progressDialog.setCancelable(false);
//                    progressDialog.show();

                    LayoutInflater inflater = LayoutInflater.from(context);
                    final View dialogLayout = inflater.inflate(R.layout.lottepin_layout, null);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setView(dialogLayout);
                    builder.setCancelable(false);

                    customAlertDialog = builder.create();
                    customAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    LottieAnimationView animationView;
                    animationView = (LottieAnimationView) dialogLayout.findViewById(R.id.animation_view);
                    animationView.setAnimation("PinJump.json");
                    animationView.loop(true);
                    animationView.playAnimation();
                    customAlertDialog.setCancelable(false);
                    customAlertDialog.show();
                    COUNT++;
                }
            });
        } catch (Exception e) {

        }
    }


    private static void cancelProgressDialog() {
        try {
//            Runnable progressRunnable = new Runnable() {
//
//                @Override
//                public void run() {
            if (customAlertDialog != null) {
                if (COUNT <= 1) {
                    customAlertDialog.cancel();
                    customAlertDialog = null;
                }


                COUNT--;
            }
//                }
//            };
//
//            Handler pdCanceller = new Handler();
//            pdCanceller.postDelayed(progressRunnable, 2000);


        } catch (Exception e) {

        }
    }

    public static void makePostRequest(final Activity context, String url, JSONObject body, boolean showDialog, final ResponseListener responseListener) {

        try {
            if (showDialog) {
                showProgressDialog(context);
            }

            JSONObject jsonObject = new JSONObject(String.valueOf(body));
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(final JSONObject response) {


                            try {
                                Runnable progressRunnable = new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            Log.d("Success Report", response.toString());

                                            responseListener.onSuccess(response.toString());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        cancelProgressDialog();

                                    }
                                };

                                Handler pdCanceller = new Handler();
                                pdCanceller.postDelayed(progressRunnable, 2000);


                            } catch (Exception e) {

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Error Report", error.toString());
                            responseListener.onError(error);
                            cancelProgressDialog();
                        }
                    }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                        volleyError = error;
                    }
                    return volleyError;
                }

            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void makePostRequest1(Context context, String url, JSONObject body, boolean showDialog, final ResponseListener responseListener) {

        try {

            JSONObject jsonObject = new JSONObject(String.valueOf(body));
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(final JSONObject response) {


                            try {
                                Runnable progressRunnable = new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            Log.d("Success Report", response.toString());

                                            responseListener.onSuccess(response.toString());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        cancelProgressDialog();

                                    }
                                };

                                Handler pdCanceller = new Handler();
                                pdCanceller.postDelayed(progressRunnable, 2000);


                            } catch (Exception e) {

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Error Report", error.toString());
                            responseListener.onError(error);
                            cancelProgressDialog();
                        }
                    }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                        volleyError = error;
                    }
                    return volleyError;
                }

            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void makeGetRequest(final Activity context, String url, boolean showDialog, final ResponseListener responseListener) {

        try {
            if (showDialog) {
                showProgressDialog(context);
            }
            JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,
                    null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        responseListener.onSuccess(response.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cancelProgressDialog();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    responseListener.onError(error);
                    error.printStackTrace();
                    cancelProgressDialog();
                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                        volleyError = error;
                    }
                    return volleyError;
                }

            };
            VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
