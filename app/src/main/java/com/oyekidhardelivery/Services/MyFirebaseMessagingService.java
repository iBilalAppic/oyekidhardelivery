package com.oyekidhardelivery.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.oyekidhardelivery.Activity.HomeActivity;
import com.oyekidhardelivery.Activity.LoginActivity;
import com.oyekidhardelivery.OtherClass.Constants;
import com.oyekidhardelivery.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Calendar;

/**
 * Created by Bilal Khan on 29/3/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static HomeActivity instance;
    String TAG = "Oye Kidhar Delivery";
    private String newMessage;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        String message = "";
        String name = "";
        String number = "";
        String serverID = "";
        String imageUrl = "";

        try {
            message = remoteMessage.getData().get(Constants.notification_key_message);
            name = remoteMessage.getData().get(Constants.notification_key_name);
            imageUrl = remoteMessage.getData().get(Constants.notification_key_image_url);
        } catch (Exception e) {
            message = "Testing";
        }

        if (newMessage == null) {
            newMessage = message;
        }
        HomeActivity base = HomeActivity.getInstance();
        boolean homeActivity = false;

        if (base != null) {
            homeActivity = base.isRunning;
        }

        if (homeActivity) {
            Intent gcm_rec = new Intent("your_action");
            LocalBroadcastManager.getInstance(base).sendBroadcast(gcm_rec);

        }

        sendNotification(new Notification(name, number, newMessage, imageUrl));


    }

    private void sendNotification(Notification messageBody) {

        Class classtoLoad = null;
        boolean addContact = false;
        classtoLoad = LoginActivity.class;


        Intent intent = new Intent(this, classtoLoad);
        intent.putExtra("noti", "notification");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

// this is a my insertion looking for a solution
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_app)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_app))
                .setContentTitle("Oye Kidhar Agent")
                .setContentText(messageBody.getMessage())
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody.getMessage()))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationID = (int) ((Calendar.getInstance().getTime().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(notificationID, notificationBuilder.build());
    }


    public class Notification {
        String name, number, message, serverID, imageUrl;

        public Notification(String name, String number, String message, String imageUrl) {
            this.name = name;
            this.number = number;
            this.message = message;
            this.imageUrl = imageUrl;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getServerID() {
            return serverID;
        }

        public void setServerID(String serverID) {
            this.serverID = serverID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
