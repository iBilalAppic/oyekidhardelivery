package com.oyekidhardelivery.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.support.annotation.Nullable;

import com.android.volley.VolleyError;
import com.oyekidhardelivery.Activity.LoginActivity;
import com.oyekidhardelivery.OtherClass.AppPreferance;
import com.oyekidhardelivery.OtherClass.Constants;
import com.oyekidhardelivery.OtherClass.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Bilal Khan on 10/11/17.
 */

public class AgentStatusService extends Service {


    AppPreferance appPreferance;
    String agentId;
    private Timer timer;
    private TimerTask task;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appPreferance = new AppPreferance();

        agentId = appPreferance.getPreferences(getApplicationContext(), Constants.agentId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        timer = new Timer();
        task = new TimerTask() {

            @Override
            public void run() {
                try {
                    final JSONObject jsonObject = new JSONObject();
                    jsonObject.put("agentid", agentId);
                    Log.e("StatusReq", jsonObject.toString());
                    RequestGenerator.makePostRequest1(getApplicationContext(), Constants.AgentStatusService, jsonObject, false, new ResponseListener() {
                        @Override
                        public void onError(VolleyError error) {

                        }

                        @Override
                        public void onSuccess(String string) throws JSONException {
                            if (!string.equals("")) {
                                JSONObject jsonObject1 = new JSONObject(string);
                                String status = jsonObject1.getString("status");
                                String msg = jsonObject1.getString("message");
                                if (status.equals("success")) {

                                } else {
                                    Utils.showToast(getApplicationContext(), "Successfully Logout");
                                    appPreferance.clearSharedPreference(getApplication());
                                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(i);
                                }
                            }
                        }
                    });


                } catch (JSONException e) {
                }
            }
        };
        timer.schedule(task, 10000, 10000);

        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        timer.cancel();
        task.cancel();
        Utils.showLog("Service Stopped");
    }
}
