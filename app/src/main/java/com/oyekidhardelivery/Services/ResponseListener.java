package com.oyekidhardelivery.Services;

import com.android.volley.VolleyError;

import org.json.JSONException;

/**
 * Created by Bilal on 30/11/2016.
 */
public interface ResponseListener {
    void onError(VolleyError error);

    void onSuccess(String string) throws JSONException;

//    ArrayList<DeliveryModel> onSuccess_one(JSONObject response);
}
